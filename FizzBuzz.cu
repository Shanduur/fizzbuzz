#include <stdio.h>

#define SIZE 101

__device__ void reverse(char str[], int length) { 
    int start = 0; 
    int end = length -1; 
    while (start < end) { 
        char tmp;
        tmp = *(str+start);
        *(str+start) = *(str+end);
        *(str+end) = tmp;
        start++; 
        end--; 
    } 
} 
  
__device__ char* cu_itoa(int num, char* str, int base) { 
    int i = 0; 
    bool isNegative = false; 
  
    if (num == 0) { 
        str[i++] = '0'; 
        return str; 
    } 
    
    if (num < 0 && base == 10) { 
        isNegative = true; 
        num = -num; 
    } 
  
    while (num != 0) { 
        int rem = num % base; 
        str[i++] = (rem > 9)? (rem-10) + 'a' : rem + '0'; 
        num = num/base; 
    } 
  
    if (isNegative) 
        str[i++] = '-'; 
    
    str[i] = ' ';

    reverse(str, i);
  
    return str; 
}

__global__ void fizzBuzz(char* d_out, int size) {
    int i = blockIdx.x;
    char fizz[] = "Fizz    \n";
    char buzz[] = "Buzz    \n";
    char fizzBuzz[] = "FizzBuzz\n";
    if (i <= SIZE) {
        if (i % 15 == 0) {
            memcpy(&d_out[i*9], &fizzBuzz, 9);
        } else if (i % 5 == 0) {
            memcpy(&d_out[i*9], &buzz, 9);
        } else if (i % 3 == 0) {
            memcpy(&d_out[i*9], &fizz, 9);
        } else {
            char buffer[10] = "        \n";
            cu_itoa(i, buffer, 10);
            memcpy(&d_out[i*9], buffer, 9);
        }
    }
}

int main() {
    char* h_out;
    char* d_out;

    h_out = (char*)malloc(SIZE*9*sizeof(char));
    cudaMalloc(&d_out, SIZE*9*sizeof(char));

    fizzBuzz<<<SIZE, 1>>>(d_out, SIZE);

    cudaMemcpy(h_out, d_out, SIZE*9*sizeof(char), cudaMemcpyDeviceToHost); 
    cudaFree(d_out);

    h_out[SIZE*9] = '\0';
    printf("%s", h_out);
}